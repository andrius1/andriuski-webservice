using System;
using Microsoft.AspNetCore.Builder;

namespace WebService.Web
{
    public static class GlobalExceptionHandlerExtensions
    {
        public static IApplicationBuilder UseGlobalExceptionHandler(this IApplicationBuilder app)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            return app.UseMiddleware<GlobalExceptionMiddleware>();
        }
    }
}