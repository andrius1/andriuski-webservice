using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Services;

namespace WebService.Web
{
    [Route("api/admin")]
    public class AdminController : BaseController
    {
        private readonly IAdminsService _adminsService;
        
        public AdminController(
            IAdminsService adminsService)
        {
            _adminsService = adminsService;
        }

        // GET api/admin/token
        [HttpGet("token")]
        [Produces("application/json")]
        public async Task<string> Login()
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.GetAsync("http://core-app-admin:5004/api/admin/token");
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }
    }
}