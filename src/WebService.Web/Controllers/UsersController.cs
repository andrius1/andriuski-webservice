using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebService.BusinessEntities;
using WebService.Services;

namespace WebService.Web
{
    [Route("api/users")]
    public class UsersController : BaseController
    {
        private readonly IUsersService _usersService;
        
        public UsersController(
            IUsersService usersService)
        {
            _usersService = usersService;
        }

        // POST api/users
        [HttpPost]
        [Produces("application/json")]
        public async Task<string> CreateUser([FromBody]UserDTOEntity user)
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
            var result = await client.PostAsync("http://core-app-users:5001/api/users", content);
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // GET api/users/id
        [HttpGet("{id}")]
        [Produces("application/json")]
        public async Task<string> GetUser(string id)
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.GetAsync($"http://core-app-users:5001/api/users/{id}");
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // GET api/users/me
        [HttpGet("me")]
        [Produces("application/json")]
        public async Task<string> GetMyUser()
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.GetAsync("http://core-app-users:5001/api/users/me");
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // GET api/users
        [HttpGet]
        [Produces("application/json")]
        public async Task<string> GetAllUsers()
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
                
            var result = await client.GetAsync("http://core-app-users:5001/api/users");
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // GET api/users/token
        [HttpGet("token")]
        [Produces("application/json")]
        public async Task<string> Login()
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.GetAsync("http://core-app-users:5001/api/users/token");
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }
    }
}