using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebService.BusinessEntities;
using WebService.Services;

namespace WebService.Web
{
    [Route("api/orders")]
    public class OrdersController : BaseController
    {
        private readonly IOrdersService _ordersService;
        
        public OrdersController(
            IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        // GET api/orders
        [HttpGet]
        [Produces("application/json")]
        public async Task<string> GetOrders()
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.GetAsync("http://core-app-orders:5003/api/orders");
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // GET api/orders/id
        [HttpGet("{id}", Name = "GetConcreteOrder")]
        [Produces("application/json")]
        public async Task<string> GetOrder(string id)
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.GetAsync($"http://core-app-orders:5003/api/orders/{id}");
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // POST api/orders
        [HttpPost]
        public async Task<string> CreateOrder([FromBody]OrderDTOEntity order)
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var content = new StringContent(JsonConvert.SerializeObject(order), Encoding.UTF8, "application/json");
            var result = await client.PostAsync("http://core-app-orders:5003/api/orders", content);
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // DELETE api/orders/id
        [HttpDelete("{id}")]
        [Produces("application/json")]
        public async Task DeleteOrder(string id)
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.DeleteAsync($"http://core-app-orders:5003/api/orders/{id}");
            Response.StatusCode = (int)result.StatusCode;
        }
    }
}