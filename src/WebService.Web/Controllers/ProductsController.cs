using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebService.BusinessEntities;
using WebService.Services;

namespace WebService.Web
{
    [Route("api/products")]
    public class ProductsController : BaseController
    {
        private readonly IProductsService _productsService;
        
        public ProductsController(
            IProductsService productsService)
        {
            _productsService = productsService;
        }
        
        // POST api/products
        [HttpPost]
        public async Task<string> CreateProduct([FromBody]ProductDTOEntity product)
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var content = new StringContent(JsonConvert.SerializeObject(product), Encoding.UTF8, "application/json");
            var result = await client.PostAsync("http://core-app-products:5002/api/products", content);
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // GET api/products
        [HttpGet]
        [Produces("application/json")]
        public async Task<string> GetAllProducts()
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.GetAsync("http://core-app-products:5002/api/products");
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // GET api/products/id
        [HttpGet("{id}", Name = "GetConcreteProduct")]
        [Produces("application/json")]
        public async Task<string> GetProduct(string id)
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.GetAsync($"http://core-app-products:5002/api/products/{id}");
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }

        // DELETE api/products/id
        [HttpDelete("{id}")]
        public async Task DeleteProduct(string id)
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var result = await client.DeleteAsync($"http://core-app-products:5002/api/users/{id}");
            Response.StatusCode = (int)result.StatusCode;
        }

        // PUT api/products/id
        [HttpPut("{id}")]
        [Produces("application/json")]
        public async Task<string> UpdateProduct(string id, [FromBody]ProductDTOEntity product)
        {
            var client = new HttpClient();
            string auth = Request.Headers["Authorization"];
            if(auth != null)
            {
                client.DefaultRequestHeaders.Remove("Authorization");
                client.DefaultRequestHeaders.Add("Authorization", auth);
            }
            var content = new StringContent(JsonConvert.SerializeObject(product), Encoding.UTF8, "application/json");
            var result = await client.PutAsync($"http://core-app-products:5002/api/products/{id}", content);
            Response.StatusCode = (int)result.StatusCode;
            
            return await result.Content.ReadAsStringAsync();
        }
    }
}