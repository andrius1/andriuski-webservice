using System;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.IdentityModel.Tokens;
using WebService.Services;

namespace WebService.Web
{
    public static class TokenProviderExtensions
    {
        public static IApplicationBuilder UseCustomJwtAuth(
            this IApplicationBuilder app, 
            JwtOptions jwtOptions, 
            string jwtSecret)
        {
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtSecret));
            
            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = jwtOptions.Issuer,

                // Validate the JWT Audience (aud) claim
                ValidateAudience = false,
                // ValidAudience = "ExampleAudience",

                // Validate the token expiry
                ValidateLifetime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero
            };
            
            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                AutomaticAuthenticate = true,
                AutomaticChallenge = true,
                TokenValidationParameters = tokenValidationParameters
            });

            return app;
        }
    }
}