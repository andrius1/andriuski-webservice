using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using WebService.BusinessEntities;

namespace WebService.DataAccess
{
    public class ProductsRepository : GenericRepository<ProductEntity>
    {
        public ProductsRepository(MongoDatabase db, string collection) : base(db, collection)
        {

        }

        public IList<ProductEntity> GetProducts()
        {
            return GetAll().ToList();
        }

        public ProductEntity GetProductById(ObjectId id)
        {
            return Get(id);
        }

        public ObjectId CreateProduct(ProductEntity product)
        {
            Create(product);
            return product.Id;
        }

        public void DeleteProductById(ObjectId id)
        {
            Remove(id);
        }

        public bool UpdateProduct(ObjectId id, ProductEntity product)
        {
            Update(id, product);
            return true;
        }
    }
}