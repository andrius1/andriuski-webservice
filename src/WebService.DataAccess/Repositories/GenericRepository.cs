using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using WebService.BusinessEntities;

namespace WebService.DataAccess
{
    public class GenericRepository<T> where T : MongoEntity
    {
        protected MongoDatabase Db { get; private set; }
        protected string Collection { get; private set; }

        public GenericRepository(MongoDatabase db, string collection)
        {
            Db = db;
            Collection = collection;
        }

        public IEnumerable<T> GetAll()
        {
            return Db.GetCollection<T>(Collection).FindAll();
        }
 
 
        public T Get(ObjectId id)
        {
            var res = Query<T>.EQ(p => p.Id, id);
            return Db.GetCollection<T>(Collection).FindOne(res);
        }
 
        public T Create(T p)
        {
            var result = Db.GetCollection<T>(Collection).Save(p);
            return p;
        }
 
        public void Update(ObjectId id, T p)
        {
            p.Id = id;
            var res = Query<T>.EQ(pd => pd.Id, id);
            var operation = Update<T>.Replace(p);
            Db.GetCollection<T>(Collection).Update(res,operation);
        }
        public void Remove(ObjectId id)
        {
            var res = Query<T>.EQ(e => e.Id, id);
            var operation = Db.GetCollection<T>(Collection).Remove(res);
        }
    }
}