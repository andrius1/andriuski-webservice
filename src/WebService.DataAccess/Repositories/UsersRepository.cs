using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using WebService.BusinessEntities;

namespace WebService.DataAccess
{
    public class UsersRepository : GenericRepository<UserEntity>
    {
        public UsersRepository(MongoDatabase db, string collection) : base(db, collection)
        {

        }

        public UserEntity GetUserById(ObjectId id)
        {
            return Get(id);
        }

        public UserEntity GetUserByName(string name)
        {
            var res = Query<UserEntity>.EQ(p => p.Name, name);
            return Db.GetCollection<UserEntity>(Collection).FindOne(res);
        }

        public IList<UserEntity> GetUsers()
        {
            return GetAll().ToList();
        }

        public ObjectId CreateUser(UserEntity user)
        {
            Create(user);
            return user.Id;
        }
    }
}