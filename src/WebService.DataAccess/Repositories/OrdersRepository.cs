using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using WebService.BusinessEntities;

namespace WebService.DataAccess
{
    public class OrdersRepository : GenericRepository<OrderEntity>
    {
        public OrdersRepository(MongoDatabase db, string collection) : base(db, collection)
        {
        }

        public IList<OrderEntity> GetOrders()
        {
            return Db.GetCollection<OrderEntity>(Collection).FindAll().ToList();
        }

        public OrderEntity GetOrderById(ObjectId id)
        {
            return Get(id);
        }

        public ObjectId CreateOrder(OrderEntity order)
        {
            Create(order);
            return order.Id;
        }

        public void DeleteOrderById(ObjectId id)
        {
            Remove(id);
        }
    }
}