using MongoDB.Driver;

namespace WebService.DataAccess
{
    public class UnitOfWork : IUnitOfWork
    {
        MongoClient _client;
        MongoServer _server;
        MongoDatabase _db;
 
        public UnitOfWork()
        {
            _client = new MongoClient("mongodb://db:27017");
            _server = _client.GetServer();
            _db = _server.GetDatabase("Orders");
        }

        private OrdersRepository _ordersRepository;
        private UsersRepository _usersRepository;
        private ProductsRepository _productsRepository;

        public virtual OrdersRepository OrdersRepository
        {
            get
            {
                if (_ordersRepository == null)
                    _ordersRepository = new OrdersRepository(_db, "Orders");
                return _ordersRepository;
            }
        }

        public virtual UsersRepository UsersRepository
        {
            get
            {
                if (_usersRepository == null)
                    _usersRepository = new UsersRepository(_db, "Users");
                return _usersRepository;
            }
        }

        public virtual ProductsRepository ProductsRepository
        {
            get
            {
                if (_productsRepository == null)
                    _productsRepository = new ProductsRepository(_db, "Products");
                return _productsRepository;
            }
        }

        public void Complete()
        {

        }
    }
}