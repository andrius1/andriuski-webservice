namespace WebService.DataAccess
{
    public interface IUnitOfWork
    {
        OrdersRepository OrdersRepository { get; }
        UsersRepository UsersRepository { get; }
        ProductsRepository ProductsRepository { get; }

        void Complete();
    }
}