using System.Collections.Generic;
using WebService.BusinessEntities;

namespace WebService.DataAccess
{
    internal static class Data
    {
        public static List<ProductEntity> Products = new List<ProductEntity>();

        public static List<UserEntity> Users = new List<UserEntity>();

        public static List<OrderEntity> Orders = new List<OrderEntity>();
    }
}