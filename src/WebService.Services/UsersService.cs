using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Bson;
using WebService.BusinessEntities;
using WebService.DataAccess;

namespace WebService.Services
{
    public class UsersService : IUsersService
    {
        private readonly JwtOptions _jwtOptions;
        private readonly IUnitOfWork _unitOfWork;

        public UsersService(IOptions<JwtOptions> jwtOptions, IUnitOfWork unitOfWork)
        {
            _jwtOptions = jwtOptions.Value;
            _unitOfWork = unitOfWork;
        }

        public IList<UserOutputEntity> GetAllUsers()
        {
            var users = _unitOfWork.UsersRepository.GetUsers();

            return users.Select(u => 
            {
                UserOutputEntity dto = new UserOutputEntity();
                dto.Id = u.Id.ToString();
                dto.Name = u.Name;
                dto.Lat = u.Lat;
                dto.Lng = u.Lng;
                return dto;
            }).ToList();
        }

        public UserOutputEntity GetUser(string id)
        {
            var u = _unitOfWork.UsersRepository.GetUserById(ObjectId.Parse(id));
            if(u == null)
                throw new ApiException(StatusCodes.Status404NotFound);
            UserOutputEntity dto = new UserOutputEntity();
            dto.Id = u.Id.ToString();
            dto.Name = u.Name;
            dto.Lat = u.Lat;
            dto.Lng = u.Lng;
            return dto;
        }

        public Token Login(string name, string password)
        {
            var user = _unitOfWork.UsersRepository.GetUserByName(name);
            if(user == null)
                throw new ApiException(ApiErrorMessages.WrongCredentials, ApiErrorCodes.WrongCredentials);
            if(user.Password != password)
                throw new ApiException(ApiErrorMessages.WrongCredentials, ApiErrorCodes.WrongCredentials);
            
            var token = GenerateToken(user.Id.ToString());
            return new Token(token, _jwtOptions.Expiration);
        }

        public Token RegisterUser(UserDTOEntity user)
        {
            if(user == null)
                throw new ApiException(ApiErrorMessages.InvalidModelState, ApiErrorCodes.InvalidModelState);

            bool exists = _unitOfWork.UsersRepository.GetUserByName(user.Name) != null;
            if(exists)
                throw new ApiException(ApiErrorMessages.UserExists, ApiErrorCodes.UserExists);
            
            ObjectId newId = _unitOfWork.UsersRepository.CreateUser(ToEntity(user));

            var token = GenerateToken(newId.ToString());
            return new Token(token, _jwtOptions.Expiration);
        }

        private string GenerateToken(string userId)
        {
            var now = DateTime.UtcNow;

            // Specifically add the jti (nonce), iat (issued timestamp), sub (subject/user) and role claims.
            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(now).ToString(), ClaimValueTypes.Integer64),
                new Claim("role", _jwtOptions.UserRole)
            };

            // Create the JWT and write it to a string
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_jwtOptions.JwtSecret));
            var jwt = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.UserAudience,
                claims: claims,
                notBefore: now,
                expires: now.AddSeconds(_jwtOptions.Expiration),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        /// <summary>
        /// Get this datetime as a Unix epoch timestamp (seconds since Jan 1, 1970, midnight UTC).
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <returns>Seconds since Unix epoch.</returns>
        private long ToUnixEpochDate(DateTime date) => 
            new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds();

        private UserEntity ToEntity(UserDTOEntity dto)
        {
            return new UserEntity
            {
                Name = dto.Name,
                Password = dto.Password,
                Lat = dto.Lat,
                Lng = dto.Lng
            };
        }
    }
}