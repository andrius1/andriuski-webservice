using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WebService.Services
{
    public class GeocodingService : IGeocodingService
    {
        private const string GEO_BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json?";
        private const string DISTANCE_BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?";

        public virtual async Task<string> ReverseGeocodeLocation(double lat, double lng)
        {
            var sb = new StringBuilder(GEO_BASE_URL)
                    .Append("latlng=")
                    .Append(lat.ToString())
                    .Append(",")
                    .Append(lng.ToString());

			string url = ToUTF8(sb.ToString());
            var client = new HttpClient();
            try
            {
                var responseJSON = await client.GetStringAsync (url);
                var response = JsonConvert.DeserializeObject<ReverseGeocodingResponseObject>(responseJSON);
                if(response.Status == "OK")
                {
                    if(response.Results.Count != 0)
                    {
                        if(response.Results[0].FormattedAddress != null)
                        {
                            return response.Results[0].FormattedAddress;
                        }
                    }
                } 
            }
            catch
            {

            }
            finally
            {
                client.Dispose();
            }
            
			return null;
        }

        public virtual async Task<int> GetDistance(double latFrom, double lngFrom, double latTo, double lngTo)
        {
            var sb = new StringBuilder(DISTANCE_BASE_URL)
                    .Append("origins=")
                    .Append(latFrom.ToString())
                    .Append(",")
                    .Append(lngFrom.ToString())
                    .Append("&")
                    .Append("destinations=")
                    .Append(latTo.ToString())
                    .Append(",")
                    .Append(lngTo.ToString());

			string url = ToUTF8(sb.ToString());
            var client = new HttpClient();
            try
            {
                var responseJSON = await client.GetStringAsync (url);
                var response = JsonConvert.DeserializeObject<DistanceResponseObject>(responseJSON);
                if(response.Status == "OK")
                {
                    if(response.Rows.Count != 0)
                    {
                        if(response.Rows[0].Elements != null)
                        {
                            if(response.Rows[0].Elements.Count != 0)
                            {
                                if(response.Rows[0].Elements[0].Distance != null)
                                {
                                    return response.Rows[0].Elements[0].Distance.Value;
                                }
                            }
                        }
                    }
                } 
            }
            catch
            {

            }
            finally
            {
                client.Dispose();
            }
            
		    return 0;
        }

        private string ToUTF8(string input)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(input);
			return Encoding.UTF8.GetString(bytes);
		}
    }
}