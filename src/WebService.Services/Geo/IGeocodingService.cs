using System.Threading.Tasks;

namespace WebService.Services
{
    public interface IGeocodingService
    {
        Task<string> ReverseGeocodeLocation(double lat, double lng);
        Task<int> GetDistance(double latFrom, double lngFrom, double latTo, double lngTo);
    }
}