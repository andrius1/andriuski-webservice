using System.Collections.Generic;
using Newtonsoft.Json;

namespace WebService.Services
{
		// Reverse geocoding

        [JsonObjectAttribute]
		public class ReverseGeocodingResponseObject
		{
			[JsonProperty("status")]
			public string Status { get; private set; }

			[JsonProperty("results")]
			public List<GeocodingResult> Results { get; private set; }
		}

		[JsonObjectAttribute]
		public class GeocodingResult
		{
			// [JsonProperty("address_components")]
			// public AddressComponent AddressComponents { get; private set; }

            [JsonProperty("formatted_address")]
            public string FormattedAddress { get; private set; }
		}

		// Distance

		public class DistanceResponseObject
		{
			[JsonProperty("status")]
			public string Status { get; private set; }

			[JsonProperty("rows")]
			public List<DistanceResult> Rows { get; private set; }
		}

		[JsonObjectAttribute]
		public class DistanceResult
		{
            [JsonProperty("elements")]
            public List<DistanceElement> Elements { get; private set; }
		}

		[JsonObjectAttribute]
		public class DistanceElement
		{
			[JsonProperty("distance")]
            public Distance Distance { get; private set; }
		}

		[JsonObjectAttribute]
		public class Distance
		{
			[JsonProperty("text")]
			public string Text { get; private set; }

			[JsonProperty("value")]
			public int Value { get; private set; }
		}
}