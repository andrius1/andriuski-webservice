namespace WebService.Services
{
    public interface IAdminsService
    {
        Token Login(string name, string password);
    }
}