using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using WebService.BusinessEntities;
using WebService.DataAccess;
using MongoDB.Bson;
using System.Linq;

namespace WebService.Services
{
    public class ProductsService : IProductsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IList<ProductDTOEntity> GetAllProducts()
        {
            return _unitOfWork.ProductsRepository.GetProducts()
                .Select(p => ToDTO(p))
                .ToList();
        }

        public ProductDTOEntity GetProduct(string id)
        {
            var product = _unitOfWork.ProductsRepository.GetProductById(ObjectId.Parse(id));
            if(product == null)
                throw new ApiException(StatusCodes.Status404NotFound); 
            return ToDTO(product);
        }

        public string CreateProduct(ProductDTOEntity product)
        {
            if(product == null)
                throw new ApiException(ApiErrorMessages.InvalidModelState, ApiErrorCodes.InvalidModelState);                
            return _unitOfWork.ProductsRepository.CreateProduct(ToEntity(product)).ToString();
        }

        public void DeleteProduct(string id)
        {
            if(_unitOfWork.ProductsRepository.GetProductById(ObjectId.Parse(id)) == null)
                throw new ApiException(StatusCodes.Status404NotFound); 
            _unitOfWork.ProductsRepository.DeleteProductById(ObjectId.Parse(id));
        }

        public bool UpdateProduct(string id, ProductDTOEntity product)
        {
            if(product == null)
                throw new ApiException(ApiErrorMessages.InvalidModelState, ApiErrorCodes.InvalidModelState); 
            
            if(_unitOfWork.ProductsRepository.GetProductById(ObjectId.Parse(id)) == null)
                throw new ApiException(StatusCodes.Status404NotFound); 

            return _unitOfWork.ProductsRepository.UpdateProduct(ObjectId.Parse(id), ToEntity(product));
        }

        private ProductDTOEntity ToDTO(ProductEntity entity)
        {
            return new ProductDTOEntity
            {
                Id = entity.Id.ToString(),
                Name = entity.Name,
                Price = entity.Price,
                Quantity = entity.Quantity
            };
        }

        private ProductEntity ToEntity(ProductDTOEntity dto)
        {
            ObjectId id;
            ObjectId.TryParse(dto.Id, out id);
            return new ProductEntity
            {
                Id = id,
                Name = dto.Name,
                Price = dto.Price,
                Quantity = dto.Quantity
            };
        }
    }
}