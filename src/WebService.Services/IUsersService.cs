using System.Collections.Generic;
using WebService.BusinessEntities;

namespace WebService.Services
{
    public interface IUsersService
    {
        Token RegisterUser(UserDTOEntity user);
        Token Login(string name, string password);
        IList<UserOutputEntity> GetAllUsers();
        UserOutputEntity GetUser(string id);
    }
}