namespace WebService.Services
{
    public static class ApiErrorMessages
    {
        public const string InvalidModelState = "Invalid model state";
        public const string UserExists = "User already exists";
        public const string WrongCredentials = "Wrong credentials";
        public const string UserDoesNotExist = "User does not exist";
        public const string InvalidLocation = "Unable to reverse geocode location";
        public const string ProductDoesNotExist = "Product does not exist";
    }
}