using System;
using Microsoft.AspNetCore.Http;

namespace WebService.Services
{
    /// <summary>
    /// Api Exception. Throw this whenever we are unable to perform the request. 
    /// </summary>
    public class ApiException : Exception
    {
        public ApiResponseData Response { get; private set; }
        public int HttpStatusCode { get; set; }

        public ApiException(int httpStatusCode)
        {
            HttpStatusCode = httpStatusCode;
        }

        public ApiException(string message, int code) : base(message)
        {
            Initialize(message, code, StatusCodes.Status400BadRequest);
        }

        public ApiException(string message, int code, int httpStatusCode) : base(message)
        {
            Initialize(message, code, httpStatusCode);
        }

        private void Initialize(string message, int code, int httpStatusCode)
        {
            Response = new ApiResponseData
            {
                Message = message,
                Code = code,
            };
            HttpStatusCode = httpStatusCode;
        }
    }

    /// <summary>
    /// This data will be formatted and returned to user.
    /// </summary>
    public class ApiResponseData
    {
        public string Message { get; set; }
        public int Code { get; set; }
    }
}