namespace WebService.Services
{
    public static class ApiErrorCodes
    {
        public const int InvalidModelState = 1000;
        public const int UserExists = 1001;
        public const int WrongCredentials = 1002;
        public const int UserDoesNotExist = 1003;
        public const int ProductDoesNotExist = 1004;
        public const int InvalidLocation = 1005;
        public const int ServerError = 5000;
    }
}