using System.Collections.Generic;
using WebService.BusinessEntities;

namespace WebService.Services
{
    public interface IProductsService
    {
        IList<ProductDTOEntity> GetAllProducts();

        ProductDTOEntity GetProduct(string id);

        string CreateProduct(ProductDTOEntity product);

        void DeleteProduct(string id);

        bool UpdateProduct(string id, ProductDTOEntity product);
    }
}