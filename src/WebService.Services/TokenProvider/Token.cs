namespace WebService.Services
{
    public class Token
    {
        public string AccessToken { get; set; }
        public int ExpiresIn { get; set; }

        public Token(string accessToken, int expiresIn)
        {
            AccessToken = accessToken;
            ExpiresIn = expiresIn;
        }
    }
}