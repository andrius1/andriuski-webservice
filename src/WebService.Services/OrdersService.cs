using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using Newtonsoft.Json;
using WebService.BusinessEntities;
using WebService.DataAccess;

namespace WebService.Services
{
    public class OrdersService : IOrdersService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGeocodingService _geocodingService;

        public OrdersService(IUnitOfWork unitOfWork, IGeocodingService geocodingService)
        {
            _unitOfWork = unitOfWork;
            _geocodingService = geocodingService;
        }

        public IList<OrderDTOEntity> GetAllOrders()
        {
            return _unitOfWork.OrdersRepository.GetOrders()
                .Select(o => ToDTO(o))
                .ToList();
        }

        public OrderDTOEntity GetOrder(string id)
        {
            var order = _unitOfWork.OrdersRepository.GetOrderById(ObjectId.Parse(id));
            if(order == null)
                throw new ApiException(StatusCodes.Status404NotFound); 
            return ToDTO(order);
        }

        public async Task<string> CreateOrder(OrderDTOEntity order, string userId)
        {
            if(order == null)
                throw new ApiException(ApiErrorMessages.InvalidModelState, ApiErrorCodes.InvalidModelState);     
            
            var user = await GetUserInternal(userId);
            if(user == null)
                throw new ApiException(ApiErrorMessages.UserDoesNotExist, ApiErrorCodes.UserDoesNotExist);   

            var product = await GetProductInternal(order.ProductId);
            if(product == null)
                throw new ApiException(ApiErrorMessages.ProductDoesNotExist, ApiErrorCodes.ProductDoesNotExist);    

            order.UserId = userId;

            // Reverse geocode user location
            var address = await _geocodingService.ReverseGeocodeLocation(user.Lat, user.Lng);
            if(address == null)
                throw new ApiException(ApiErrorMessages.InvalidLocation, ApiErrorCodes.InvalidLocation);   
            
            double startLat = 54.729233;
            double startLng = 25.262748;
            int distance = await _geocodingService.GetDistance(startLat, startLng, user.Lat, user.Lng);
            
            order.Address = address;
            order.Distance = distance;
                       
            return _unitOfWork.OrdersRepository.CreateOrder(ToEntity(order)).ToString();
        }

        public void DeleteOrder(string id)
        {
            if(_unitOfWork.OrdersRepository.GetOrderById(ObjectId.Parse(id)) == null)
                throw new ApiException(StatusCodes.Status404NotFound); 
            _unitOfWork.OrdersRepository.DeleteOrderById(ObjectId.Parse(id));
        }

        private OrderDTOEntity ToDTO(OrderEntity entity)
        {
            return new OrderDTOEntity
            {
                Id = entity.Id.ToString(),
                UserId = entity.UserId.ToString(),
                ProductId = entity.ProductId.ToString(),
                OrderDate = entity.OrderDate,
                Distance = entity.Distance,
                Address = entity.Address
            };
        }

        private OrderEntity ToEntity(OrderDTOEntity dto)
        {
            ObjectId id;
            ObjectId.TryParse(dto.Id, out id);
            return new OrderEntity
            {
                Id = id,
                UserId = ObjectId.Parse(dto.UserId),
                ProductId = ObjectId.Parse(dto.ProductId),
                OrderDate = dto.OrderDate,
                Distance = dto.Distance,
                Address = dto.Address
            };
        }

        // ========= Http calls ========= //

        public async Task<UserOutputEntity> GetUserInternal(string id)
        {
            var client = new HttpClient();
            try
            {
                var result = await client.GetAsync($"http://core-app-users:5001/api/users/internal/{id}");
                return JsonConvert.DeserializeObject<UserOutputEntity>(await result.Content.ReadAsStringAsync());
            }
            catch
            {
                return null;
            }
        }

        public async Task<ProductDTOEntity> GetProductInternal(string id)
        {
            var client = new HttpClient();
            try
            {
                var result = await client.GetAsync($"http://core-app-products:5002/api/products/internal/{id}");
                return JsonConvert.DeserializeObject<ProductDTOEntity>(await result.Content.ReadAsStringAsync());
            }
            catch
            {
                return null;
            }
        }
    }
}