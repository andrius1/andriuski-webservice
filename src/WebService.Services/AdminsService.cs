using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace WebService.Services
{
    public class AdminsService : IAdminsService
    {
        private readonly JwtOptions _jwtOptions;

        public AdminsService(IOptions<JwtOptions> jwtOptions)
        {
            _jwtOptions = jwtOptions.Value;
        }

        public Token Login(string name, string password)
        {
            if(name == "admin" && password == "password")
            {
                var token = GenerateToken("1");
                return new Token(token, _jwtOptions.Expiration);
            }
            throw new ApiException(ApiErrorMessages.WrongCredentials, ApiErrorCodes.WrongCredentials);
        }

        private string GenerateToken(string userId)
        {
            var now = DateTime.UtcNow;

            // Specifically add the jti (nonce), iat (issued timestamp), sub (subject/user) and role claims.
            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userId),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(now).ToString(), ClaimValueTypes.Integer64),
                new Claim("role", _jwtOptions.AdminRole)
            };

            // Create the JWT and write it to a string
            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_jwtOptions.JwtSecret));
            var jwt = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.AdminAudience,
                claims: claims,
                notBefore: now,
                expires: now.AddSeconds(_jwtOptions.Expiration),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        /// <summary>
        /// Get this datetime as a Unix epoch timestamp (seconds since Jan 1, 1970, midnight UTC).
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <returns>Seconds since Unix epoch.</returns>
        private long ToUnixEpochDate(DateTime date) => 
            new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds();
    }
}