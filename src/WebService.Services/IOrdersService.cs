using System.Collections.Generic;
using System.Threading.Tasks;
using WebService.BusinessEntities;

namespace WebService.Services
{
    public interface IOrdersService
    {
        IList<OrderDTOEntity> GetAllOrders();

        OrderDTOEntity GetOrder(string id);

        Task<string> CreateOrder(OrderDTOEntity order, string userId);

        void DeleteOrder(string id);
    }
}