namespace WebService.Services
{
    public class JwtOptions
    {
        public int Expiration { get; set; } = 7776000;
        public string Issuer { get; set; } = "api.webservice.com";
        public string UserAudience { get; set; } = "api.webservice.com";
        public string AdminAudience { get; set; } = "api.webservice.com";
        public string UserRole { get; set; } = Roles.User;
        public string AdminRole { get; set; } = Roles.Admin;
        public string JwtSecret { get; set; }
    }
}