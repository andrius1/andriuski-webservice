using System;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Services;

namespace WebService.Web
{
    [Route("api/admin")]
    public class AdminController : BaseController
    {
        private readonly IAdminsService _adminsService;
        
        public AdminController(
            IAdminsService adminsService)
        {
            _adminsService = adminsService;
        }

        // GET api/admin/token
        [HttpGet("token")]
        public IActionResult Login()
        {
            string auth = Request.Headers["Authorization"];
            if(auth == null)
                throw new ApiException(StatusCodes.Status400BadRequest);
            
            string username = string.Empty;
            string password = string.Empty;

            try
            {
                string encodedUsernamePassword = auth.Substring("Basic ".Length).Trim();
                byte[] data = Convert.FromBase64String(encodedUsernamePassword);
                string decodedCredentials = Encoding.UTF8.GetString(data);
                int seperatorIndex = decodedCredentials.IndexOf(':');
                username = decodedCredentials.Substring(0, seperatorIndex);
                password = decodedCredentials.Substring(seperatorIndex + 1);
            }
            catch
            {
                throw new ApiException(StatusCodes.Status400BadRequest);
            }
            

            var token = _adminsService.Login(username, password);
            return Ok(token);
        }
    }
}