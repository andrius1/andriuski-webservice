using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.Extensions.Logging;
using WebService.Services;

namespace WebService.Web
{
    public class GlobalExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly ObjectResultExecutor _oex;

        public GlobalExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory, ObjectResultExecutor oex)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<GlobalExceptionMiddleware>();
            _oex = oex;
        }

        public async Task Invoke(HttpContext context)
        {
            try 
            {
                await _next.Invoke(context);
            } 
            catch (Exception ex) 
            {
                if (context.Response.HasStarted) 
                {
                    _logger.LogWarning("The response has already started, the global exception middleware will not be executed");
                    throw;
                }

                context.Response.Clear();
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;

                object response = new ApiResponseData { Message = "Internal Server Error", Code = ApiErrorCodes.ServerError };

                if(ex is ApiException)
                {
                    var apiEx = (ApiException)ex;
                    context.Response.StatusCode = apiEx.HttpStatusCode;
                    response = apiEx.Response;
                }
                else if(ex is AggregateException)
                {
                    if(ex.InnerException is ApiException)
                    {
                        var apiEx = (ApiException)ex.InnerException;
                        context.Response.StatusCode = apiEx.HttpStatusCode;
                        response = apiEx.Response;
                    }
                }
                else
                {
                    _logger.LogError(5000, $"Non ApiException occured: {ex.Message}{Environment.NewLine}{ex.StackTrace}");
                }
                
                await _oex.ExecuteAsync(
                    new ActionContext() { HttpContext = context }, 
                    new ObjectResult(response));
           }
        }
    }
}