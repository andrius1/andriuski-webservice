using System;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.BusinessEntities;
using WebService.Services;

namespace WebService.Web
{
    [Route("api/users")]
    public class UsersController : BaseController
    {
        private readonly IUsersService _usersService;
        
        public UsersController(
            IUsersService usersService)
        {
            _usersService = usersService;
        }

        // POST api/users
        [HttpPost]
        public IActionResult CreateUser([FromBody]UserDTOEntity user)
        {
            EnsureModelStateIsValid();
            var token = _usersService.RegisterUser(user);
            return Ok(token);
        }

        // GET api/users/id
        [HttpGet("{id}")]
        [Authorize(Roles = Roles.Admin)]
        public IActionResult GetUser(string id)
        {
            var user = _usersService.GetUser(id);
            return Ok(user);
        }

        // GET api/users/me
        [HttpGet("me")]
        [Authorize(Roles = Roles.User)]
        public IActionResult GetMyUser()
        {
            var user = _usersService.GetUser(UserId);
            return Ok(user);
        }

        // GET api/users
        [HttpGet]
        [Authorize(Roles = Roles.Admin)]
        public IActionResult GetAllUsers()
        {
            System.Console.WriteLine("Inside users service");
            var users = _usersService.GetAllUsers();
            System.Console.WriteLine(users.Count);
            return Ok(users);
        }

        // GET api/users/token
        [HttpGet("token")]
        public IActionResult Login()
        {
            string auth = Request.Headers["Authorization"];
            if(auth == null)
                throw new ApiException(StatusCodes.Status400BadRequest);
            
            string username = string.Empty;
            string password = string.Empty;

            try
            {
                string encodedUsernamePassword = auth.Substring("Basic ".Length).Trim();
                byte[] data = Convert.FromBase64String(encodedUsernamePassword);
                string decodedCredentials = Encoding.UTF8.GetString(data);
                int seperatorIndex = decodedCredentials.IndexOf(':');
                username = decodedCredentials.Substring(0, seperatorIndex);
                password = decodedCredentials.Substring(seperatorIndex + 1);
            }
            catch
            {
                throw new ApiException(StatusCodes.Status400BadRequest);
            }

            var token = _usersService.Login(username, password);
            return Ok(token);
        }

        // ========= Internal endpoints ========= //

        // GET api/users/internal/id
        [HttpGet("internal/{id}")]
        public IActionResult GetUserInternal(string id)
        {
            var user = _usersService.GetUser(id);
            return Ok(user);
        }
    }
}