using System;

using Microsoft.AspNetCore.Mvc.ModelBinding;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace WebService.BusinessEntities
{
    [BindRequired]
    public class OrderEntity : MongoEntity
    {
        //public int Id { get; set; }
        [BsonElement("UserId")]
        public ObjectId UserId { get; set; }
        [BsonElement("ProductId")]
        public ObjectId ProductId { get; set; }
        [BsonElement("OrderDate")]
        public DateTime OrderDate { get; set; }
        [BsonElement("Distance")]
        public int Distance { get; set; }
        [BsonElement("Address")]
        public string Address { get; set; }
    }
}