using System.ComponentModel.DataAnnotations;

namespace WebService.BusinessEntities
{
    public class UserEntity : MongoEntity
    {
        //public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}