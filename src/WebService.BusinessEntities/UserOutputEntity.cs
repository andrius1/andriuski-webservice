namespace WebService.BusinessEntities
{
    public class UserOutputEntity
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}