using MongoDB.Bson;

namespace WebService.BusinessEntities
{
    public class MongoEntity
    {
        public ObjectId Id { get; set; }
    }
}