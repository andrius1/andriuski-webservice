using System;

namespace WebService.BusinessEntities
{
    public class OrderDTOEntity
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public string ProductId { get; set; }
        public DateTime OrderDate { get; set; }
        public int Distance { get; set; }
        public string Address { get; set; }
    }
}