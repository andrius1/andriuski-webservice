using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebService.BusinessEntities;
using WebService.Services;

namespace WebService.Web
{
    [Route("api/products")]
    public class ProductsController : BaseController
    {
        private readonly IProductsService _productsService;
        
        public ProductsController(
            IProductsService productsService)
        {
            _productsService = productsService;
        }
        
        // POST api/products
        [HttpPost]
        [Authorize(Roles = Roles.Admin)]
        public IActionResult CreateProduct([FromBody]ProductDTOEntity product)
        {
            EnsureModelStateIsValid();
            string id = _productsService.CreateProduct(product);
            return CreatedAtRoute("GetConcreteProduct", new { id = id }, id);
        }

        // GET api/products
        [HttpGet]
        //[Authorize(Roles = Roles.AdminUser)]
        public IActionResult GetAllProducts()
        {
            var products = _productsService.GetAllProducts();
            return Ok(products);
        }

        // GET api/products/id
        [HttpGet("{id}", Name = "GetConcreteProduct")]
        //[Authorize(Roles = Roles.AdminUser)]
        public IActionResult GetProduct(string id)
        {
            var product = _productsService.GetProduct(id);
            return Ok(product);
        }

        // DELETE api/products/id
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.Admin)]
        public IActionResult DeleteProduct(string id)
        {
            _productsService.DeleteProduct(id);
            return Ok();
        }

        // PUT api/products/id
        [HttpPut("{id}")]
        [Authorize(Roles = Roles.Admin)]
        public IActionResult UpdateProduct(string id, [FromBody]ProductDTOEntity product)
        {
            EnsureModelStateIsValid();
            bool success = _productsService.UpdateProduct(id, product);
            if(success)
                return Ok();
            else
                return BadRequest();
        }

        // ========= Internal endpoints ========= //

        // GET api/products/internal/id
        [HttpGet("internal/{id}")]
        public IActionResult GetProductInternal(string id)
        {
            var product = _productsService.GetProduct(id);
            return Ok(product);
        }
    }
}