using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace WebService.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
           var config = new ConfigurationBuilder()
                .AddCommandLine(args)
                .AddEnvironmentVariables(prefix: "ASPNETCORE_")
                .Build();

            var host = new WebHostBuilder()
                .UseUrls("http://0.0.0.0:5003")
                .UseConfiguration(config)
                .UseKestrel()
                .UseContentRoot(GetContentRootDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }

        /// <summary>
        /// If we are runing 'dotnet run' command from solution level directory this method
        /// will append project's name to the end.
        /// </summary>
        private static string GetContentRootDirectory()
        {
            string directory = Directory.GetCurrentDirectory();
            string project = Assembly.GetEntryAssembly().GetName().Name;

            if(Path.GetFileName(directory) != Assembly.GetEntryAssembly().GetName().Name)
            {
                return Path.Combine(directory, "src", project);
            }
            
            return directory;
        }
    }
}
