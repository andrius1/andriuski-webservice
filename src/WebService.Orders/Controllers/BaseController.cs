using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebService.Services;

namespace WebService.Web
{
    public class BaseController : Controller
    {
        /// <summary>
        /// Returns NameIdentifier claim of currently signed in user.
        /// </summary>
        protected string UserId 
        {
            get
            {
                return User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            }
        }

        /// <summary>
        /// This method will throw ApiException if model state is invalid.
        /// </summary>
        protected void EnsureModelStateIsValid()
        {
            string errorMessage = ModelState
                .SelectMany(x => x.Value?.Errors)
                ?.FirstOrDefault()
                ?.ErrorMessage ?? ApiErrorMessages.InvalidModelState;
            
            if(!ModelState.IsValid)
                throw new ApiException(
                    errorMessage, 
                    ApiErrorCodes.InvalidModelState, 
                    StatusCodes.Status400BadRequest);
        }
    }
}