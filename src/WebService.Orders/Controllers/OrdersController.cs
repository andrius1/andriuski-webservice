using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebService.BusinessEntities;
using WebService.Services;

namespace WebService.Web
{
    [Route("api/orders")]
    public class OrdersController : BaseController
    {
        private readonly IOrdersService _ordersService;
        
        public OrdersController(
            IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        // GET api/orders
        [HttpGet]
        [Authorize(Roles = Roles.AdminUser)]
        public IActionResult GetOrders()
        {
            var orders = _ordersService.GetAllOrders();
            return Ok(orders);
        }

        // GET api/orders/id
        [HttpGet("{id}", Name = "GetConcreteOrder")]
        [Authorize(Roles = Roles.AdminUser)]
        public IActionResult GetOrder(string id)
        {
            var order = _ordersService.GetOrder(id);
            return Ok(order);
        }

        // POST api/orders
        [HttpPost]
        [Authorize(Roles = Roles.User)]
        public async Task<IActionResult> CreateOrder([FromBody]OrderDTOEntity order)
        {
            EnsureModelStateIsValid();
            string id = await _ordersService.CreateOrder(order, UserId);
            return CreatedAtRoute("GetConcreteOrder", new { id = id }, id);
        }

        // DELETE api/orders/id
        [HttpDelete("{id}")]
        [Authorize(Roles = Roles.AdminUser)]
        public IActionResult DeleteOrder(string id)
        {
            _ordersService.DeleteOrder(id);
            return Ok();
        }
    }
}