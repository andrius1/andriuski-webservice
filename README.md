# How to run:

git pull

**Build both WebApi and Mongodb containers:**

*   docker-compose up --build

**Build just WebApi:**

*   docker build -t andriuski/webservice:0.9.0 .
*   docker run -p 5000:5000 -d andriuski/webservice:0.9.0


# Endpoints and allowed methods:

/api/orders					  GET, POST,  
/api/orders/123				GET, DELETE  

/api/users					  GET, POST  
/api/users/123				GET  
/api/users/token			GET  

/api/products				  GET, POST  
/api/products/123			GET, PUT, DELETE  

/api/admin/token			GET  

# Data transfer objects:
**User:**
`{
  "Name": "vardas",
  "Password": "1234",
  "Lat": 54.712830,
  "Lng": 25.272399
}`

**Product:**
`{
  "Id": "id",
  "Name": "vardas",
  "Price": 0.0,
  "Quantity": 0
}`

**Order:**
`{
  "UserId": "id",
  "ProductId": "id",
  "OrderDate": "2017-01-01T00:00:00"
}`


Use basic authentication to create account or login.
Use token received at account creation for every subsequent request. Sample:

Authorization: Bearer <token>